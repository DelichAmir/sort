import { Sorter } from './Sorter';
import { NumbersCollection } from './NumbersCollection';
import { CharactersCollections } from './CharactersCollections';
import { LinkedList } from './LinkedList';

const numbers = new NumbersCollection([50, 3, -5, 0]);
numbers.sort();
console.log('Sorted numbers: ', numbers.data);

const characters = new CharactersCollections('Xyzabd');
characters.sort();
console.log('Sorted characters: ', characters.data);

const linkedList = new LinkedList();
linkedList.add(500);
linkedList.add(-10);
linkedList.add(-3);
linkedList.add(4);
linkedList.sort();
linkedList.print();
